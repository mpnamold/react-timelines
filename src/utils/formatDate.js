export const getMonth = date => "tháng " + (date.getMonth()+1);

export const getDayMonth = date => `${date.getDate()} ${getMonth(date)}`
